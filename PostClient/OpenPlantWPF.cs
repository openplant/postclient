﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Globalization;
using Microsoft.Win32;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Media;
using System.Windows;

/*
    The following Assemblies need to be added
    -   PresentationCore
    -   PresentationFramework
    -   WindowsBase
*/

namespace PostClient
{
    #region VUTILITIES (WPF) ********************************************************************************************************
    //Visual Related Utilities
    public class VUtilities
    {
        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        [DebuggerStepThroughAttribute]
        public static T FindChildByName<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChildByName<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        [DebuggerStepThroughAttribute]
        public IEnumerable<Child> FindAllLogicalChildren<Child>(DependencyObject Parent) where Child : DependencyObject
        {
            if (Parent != null)
            {
                foreach (object rawChild in LogicalTreeHelper.GetChildren(Parent))
                {
                    if (rawChild is DependencyObject)
                    {
                        DependencyObject child = (DependencyObject)rawChild;
                        if (child is Child)
                        {
                            yield return (Child)child;
                        }

                        foreach (Child childOfChild in FindAllLogicalChildren<Child>(child))
                        {
                            yield return childOfChild;
                        }
                    }
                }
            }
        }

        [DebuggerStepThroughAttribute]
        public IEnumerable<Child> FindAllVisualChildren<Child>(DependencyObject Parent) where Child : DependencyObject
        {
            if (Parent != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(Parent); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(Parent, i);
                    if (child != null && child is Child)
                    {
                        yield return (Child)child;
                    }

                    foreach (Child childOfChild in FindAllVisualChildren<Child>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        public static IEnumerable<Child> FindAllVisualChildren_Static<Child>(DependencyObject Parent) where Child : DependencyObject
        {
            if (Parent != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(Parent); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(Parent, i);
                    if (child != null && child is Child)
                    {
                        yield return (Child)child;
                    }

                    foreach (Child childOfChild in FindAllVisualChildren_Static<Child>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        [DebuggerStepThroughAttribute]
        public DependencyObject FindFirstVisualChildren(DependencyObject Parent, Type ChildType)
        {
            if (Parent != null)
            {
                if (Parent.GetType() == ChildType)
                {
                    return Parent;
                }

                int ChildCount = VisualTreeHelper.GetChildrenCount(Parent);
                for (int i = 0; i < ChildCount; i++)
                {
                    DependencyObject childReturn = FindFirstVisualChildren(VisualTreeHelper.GetChild(Parent, i), ChildType);
                    if (childReturn != null)
                    {
                        return childReturn;
                    }
                }
            }

            return null;
        }

        public Visual GetDescendantByType(Visual element, Type type)
        {
            if (element == null) return null;
            if (element.GetType() == type) return element;
            Visual foundElement = null;
            if (element is FrameworkElement)
                (element as FrameworkElement).ApplyTemplate();
            for (int i = 0;
                i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                Visual visual = VisualTreeHelper.GetChild(element, i) as Visual;
                foundElement = GetDescendantByType(visual, type);
                if (foundElement != null)
                    break;
            }
            return foundElement;
        }

        [DebuggerStepThroughAttribute]
        public static ParentType FindVisualParent<ParentType>(DependencyObject child) where ParentType : DependencyObject
        {
            // get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            // we’ve reached the end of the tree
            if (parentObject == null) return null;

            // check if the parent matches the type we’re looking for
            ParentType parent = parentObject as ParentType;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                // use recursion to proceed with next level
                return FindVisualParent<ParentType>(parentObject);
            }
        }

        [DebuggerStepThroughAttribute]
        public static ParentType FindLogicalParent<ParentType>(DependencyObject child) where ParentType : DependencyObject
        {
            // get parent item
            DependencyObject parentObject = LogicalTreeHelper.GetParent(child);

            // we’ve reached the end of the tree
            if (parentObject == null) return null;

            // check if the parent matches the type we’re looking for
            ParentType parent = parentObject as ParentType;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                // use recursion to proceed with next level
                return FindLogicalParent<ParentType>(parentObject);
            }
        }
    }
    
    #endregion


    #region EXTENSIONS **************************************************************************************************************
    public static partial class Extensions
    {
        /// <summary>
        /// Finds a parent of a given item on the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="child">A direct or indirect child of the
        /// queried item.</param>
        /// <returns>The first parent item that matches the submitted
        /// type parameter. If not matching item can be found, a null
        /// reference is being returned.</returns>
        public static T TryFindParent<T>(this DependencyObject child)
            where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = GetParentObject(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                //use recursion to proceed with next level
                return TryFindParent<T>(parentObject);
            }
        }

        /// <summary>
        /// This method is an alternative to WPF's
        /// <see cref="VisualTreeHelper.GetParent"/> method, which also
        /// supports content elements. Keep in mind that for content element,
        /// this method falls back to the logical tree of the element!
        /// </summary>
        /// <param name="child">The item to be processed.</param>
        /// <returns>The submitted item's parent, if available. Otherwise
        /// null.</returns>
        public static DependencyObject GetParentObject(this DependencyObject child)
        {
            if (child == null) return null;

            //handle content elements separately
            ContentElement contentElement = child as ContentElement;
            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null) return parent;

                FrameworkContentElement fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            //also try searching for parent in framework elements (such as DockPanel, etc)
            FrameworkElement frameworkElement = child as FrameworkElement;
            if (frameworkElement != null)
            {
                DependencyObject parent = frameworkElement.Parent;
                if (parent != null) return parent;
            }

            //if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child);
        }
    }
    #endregion



}
