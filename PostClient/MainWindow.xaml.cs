﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PostClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Post post = new Post();
            
            string someStringFromColumnZero = "";


            ObservableCollection<Post> PostsFromDatabase = new ObservableCollection<Post>();
            


            //Get data from db
            var dbCon = DBConnection.Instance();
            dbCon.DatabaseName = "eculight";
            if (dbCon.IsConnect())
            {
                //suppose col0 and col1 are defined as VARCHAR in the DB
                //string query = "SELECT `post_content` FROM `wp_posts` WHERE `ID` = 48; ";
                string query = "SELECT * FROM `wp_posts`";
                var queryExecution = new MySqlCommand(query, dbCon.Connection);
                var result = queryExecution.ExecuteReader();
                while (result.Read())
                {
                    //someStringFromColumnZero = result.GetString(0);
                    string content = result.GetString("post_content");
                    PostsFromDatabase.Add(new Post() { PostContent = result.GetString("post_content"), PostName = result.GetString("post_title") });
                }
            }
            //PostsFromDatabase.Add(new Post() { PostName = someStringFromColumnZero, PostContent = someStringFromColumnZero });

            PostClientWindow1ViewModel postClientWindow1ViewModel = new PostClientWindow1ViewModel()
            {
                Posts = PostsFromDatabase
            };

            this.DataContext = postClientWindow1ViewModel;

        }
    }



    public class DBConnection
    {
        private DBConnection()
        {
        }

        private string databaseName = string.Empty;
        public string DatabaseName
        {
            get { return databaseName; }
            set { databaseName = value; }
        }

        public string Password { get; set; }
        private MySqlConnection connection = null;
        public MySqlConnection Connection
        {
            get { return connection; }
        }

        private static DBConnection _instance = null;
        public static DBConnection Instance()
        {
            if (_instance == null)
                _instance = new DBConnection();
            return _instance;
        }

        public bool IsConnect()
        {
            bool result = true;
            if (Connection == null)
            {
                if (String.IsNullOrEmpty(databaseName))
                    result = false;
                string connstring = string.Format("Server=localhost; database={0}; UID=root; password=", databaseName);
                connection = new MySqlConnection(connstring);
                connection.Open();
                result = true;
            }

            return result;
        }

        public void Close()
        {
            connection.Close();
        }
    }


    public class Post
    {
        public string PostName { get; set; }
        public string PostContent { get; set; }
    }

    [ServiceContract]
    public interface IPostContract
    {
        [OperationContract]
        bool SendPost(ObservableCollection<Post> Posts);

        [OperationContract]
        string SendTestMessage(string msg);
    }


    public class PostClientWindow1ViewModel : INotifyPropertyChanged
    {
        #region BOILER PLATE
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
        #endregion

        ObservableCollection<Table> _Tables = new ObservableCollection<Table>();
        public ObservableCollection<Table> Tables { get { return _Tables; } set { SetField(ref _Tables, value, "Tables"); } }
        ObservableCollection<Post> _Posts = new ObservableCollection<Post>();
        public ObservableCollection<Post> Posts { get { return _Posts; } set { SetField(ref _Posts, value, "Posts"); } }

    }
}
